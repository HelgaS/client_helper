import React, {useEffect} from 'react';
import './RegisterForm.css';
import {useFormik} from "formik";
import * as yup from 'yup';
import store from "./sagas/store";
import InputComponent from './inputComponent.js';

import {useSelector, useDispatch} from "react-redux";
import {getContactsStart, authContactsStart} from "./sagas/actions";


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faAngleRight } from '@fortawesome/free-solid-svg-icons'


export default function RegisterForm() {

    const {contacts: data} = useSelector((state) => state.date);
    // const {message: message} = useSelector((state) => state.data.message);
    let dispatch = useDispatch();

    const validationSchema = yup.object().shape({
        email: yup.string().email('Введите корректный e-mail').required('Обязательно к заполнению'),
        password: yup.string().typeError('Должно быть строкой').required('Обязательно к заполнению')
    });

    useEffect(() =>{
        dispatch(getContactsStart());
        console.log('working!');
    }, []);

    const formik = useFormik({
        initialValues:{
            email: '',
            password: '',
            message: ''
        },
        onSubmit:(values) => {
            console.log(values);
            console.log("contacts", {contacts: data});
            dispatch(authContactsStart({values, contacts: data}));
            console.log("contacts mes", store.getState().date.message);
        },
        validationSchema:validationSchema
    })

    return (
        <form onSubmit={formik.handleSubmit} className={'registerForm'}>
            <div className={'form'}>
                <div className={'form-title'}>
                <p >Войти в свой аккаунт</p>

                </div>
                <div className={'form-input-components'}>
                    <InputComponent name={'email'} type={'text'} label={'E-mail'} handleChange={formik.handleChange} handleBlur={formik.handleBlur} value={formik.values.email} touched={formik.touched.email} errors={formik.errors.email} />
                    <InputComponent name={'password'} type={'password'} label={'Пароль'} handleChange={formik.handleChange} handleBlur={formik.handleBlur} value={formik.values.password} touched={formik.touched.password} errors={formik.errors.password} />
                </div>
                {store.getState().date.message === "fail" && <p className={'form-error'}>{"Введены неверные логин или пароль"}</p>}

                <button
                    className={'form-button'}
                    disabled={!formik.isValid && !formik.dirty}
                    onClick={formik.handleSubmit}
                    type={'submit'}
                >Войти
                    <FontAwesomeIcon style={{marginLeft: '0.5em'}} icon={faAngleRight} /></button>
            </div>
        </form>
    ) ;


}

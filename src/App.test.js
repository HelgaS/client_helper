import { render, screen } from '@testing-library/react';
import App from './App';
import {shallow} from "enzyme";
import RegisterForm from "./RegisterForm";
import {expect} from "chai";
import React from "react";

describe('<RegisterForm />', () => {

  it('renders button', () => {
    const wrapper = shallow(<RegisterForm />);
    expect(wrapper.find('button')).toHaveLength(1);
  });

});
import firebase from "firebase/compat/app";
import { getDatabase, ref, onValue} from "firebase/database";
import 'firebase/compat/auth';
import 'firebase/compat/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyAb5pSNGbcjnrsM3Do5nOgyAO-OOj6kPgI",
    authDomain: "client-helper-ff7ae.firebaseapp.com",
    projectId: "client-helper-ff7ae",
    storageBucket: "client-helper-ff7ae.appspot.com",
    messagingSenderId: "762342836621",
    appId: "1:762342836621:web:5a3adc14fecbadc2bcd5c4",
    measurementId: "G-4GEF7PYVDG",
    databaseURL: "https://client-helper-ff7ae-default-rtdb.firebaseio.com",
};

// Initialize Firebase
const firebaseApp = firebase.initializeApp(firebaseConfig);
const firebaseDatabase = firebaseApp.firestore();
// const auth = firebaseApp.auth();
const db = getDatabase();
const usersRef = ref(db, 'users');

export default usersRef;
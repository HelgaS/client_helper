import React, { Component } from 'react';
import './RegisterForm.css';

class InputComponent extends Component {
    constructor(props){
        super(props);
    }

    render() {
        return (
            <div>
                <p>
                    <label htmlFor={this.props.name}>{this.props.label}</label>
                    <input
                        className={'form-input'}
                        type={this.props.type}
                        name={this.props.name}
                        onChange={this.props.handleChange}
                        onBlur={this.props.handleBlur}
                        value={this.props.value}
                    />
                </p>
                {this.props.touched && this.props.errors && <p className={'form-error'}>{this.props.errors}</p>}
            </div>
        );
    }
}

export default InputComponent;
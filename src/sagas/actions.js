

export const getContactsStart = () => ({
    type: 'FETCH_MESSAGES_REQUEST'
});

export const getContactsSuccess = (contacts) => ({
    type: 'FETCH_MESSAGES_SUCCESS',
    payload: contacts
});

export const getContactsFail = (error) => ({
    type: 'FETCH_MESSAGES_FAILURE',
    payload: error
});

export const authContactsStart = (data) => ({
    type: 'AUTH_MESSAGES_REQUEST',
    payload: data
});

export const authContactsSuccess = (message) => ({
    type: 'AUTH_MESSAGES_SUCCESS',
    message: message
});

export const authContactsFail = (error) => ({
    type: 'AUTH_MESSAGES_FAILURE',
    payload: error
});
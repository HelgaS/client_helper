import {combineReducers} from "redux";
import contactReducer from "./reducers";

const rootReducer = combineReducers({date: contactReducer,});

export default rootReducer;
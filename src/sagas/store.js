import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'

import rootReducer from './root-reducer'
import rootSaga from './sagas'
import logger from "redux-logger"

// создаем saga мидлвар
const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware];
if(process.env.NODE_ENV === "development"){
    middleware.push(logger);
}
// монтируем его в Store
const store = createStore(
    rootReducer,
    applyMiddleware(...middleware)
);

// затем запускаем сагу
sagaMiddleware.run(rootSaga);

export default store;
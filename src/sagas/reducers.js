

const initialState = {
  contacts: {},
  loading: false,
  error: false,
  message: {}
};

export default function counter(state = initialState, action) {
  switch (action.type) {
    case 'FETCH_MESSAGES_REQUEST':
    case 'AUTH_MESSAGES_REQUEST':
      return {
       ...state,
        loading: true
      };
    case 'FETCH_MESSAGES_SUCCESS':
      return {
        ...state,
        contacts: action.payload,
        loading: false
      };
    case 'FETCH_MESSAGES_FAILURE':
    case 'AUTH_MESSAGES_FAILURE':
      return {
        ...state,
        error: action.payload,
        loading: false
      };
    case 'AUTH_MESSAGES_SUCCESS':
      return {
        ...state,
        loading: false,
        message: action.message
      };
    default:
      return state;
  }
}

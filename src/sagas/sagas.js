import { all, call, delay, put, takeEvery, takeLatest, fork } from 'redux-saga/effects';
import firebase from "../firebase";
import { getDatabase, ref, onValue} from "firebase/database";
import 'firebase/compat/auth';
import 'firebase/compat/firestore';
import { getContactsSuccess, getContactsFail, authContactsSuccess, authContactsFail} from "./actions";

export function* onLoadUsersAsync() {
  const delay = (ms) => new Promise(res => setTimeout(res, ms));

  try {
    var data = null;
    yield onValue(firebase, (snapshot) => {
        console.log("In process");
        data = snapshot.val();
      });
    console.log(data);
    yield delay(2000);
    if(data !== null){
      yield put(getContactsSuccess(data));
    }else{
      yield put(getContactsSuccess({}));
      console.log("Empty");
    }
  } catch (error) {
    console.log(error);
    yield put(getContactsFail());
  }
}

export function* onLoadUsers() {
  yield takeLatest('FETCH_MESSAGES_REQUEST', onLoadUsersAsync)
}

export function* onAuthUserAsync({payload}) {
  try {
  console.log("payload", payload);
      const index = payload.contacts.findIndex(todo => todo.email === payload.values.email);

      if (index !== -1 && payload.contacts[index].password === payload.values.password){
        console.log("Найден пользователь с почтой " + payload.values.email);
        yield put(authContactsSuccess("Авторизация прошла успешно"));
      }else{
        console.log("Данный пользователь не найден");
        yield put(authContactsSuccess("fail"));
      }
  } catch (error) {
    console.log(error);
    yield put(authContactsFail());
  }
}

export function* onAuthUser() {
  yield takeLatest('AUTH_MESSAGES_REQUEST', onAuthUserAsync)
}


const contactSagas = [fork(onLoadUsers), fork(onAuthUser)]
// single entry point to start all Sagas at once
export default function* rootSaga() {
  yield all([
      ...contactSagas
  ]);
}

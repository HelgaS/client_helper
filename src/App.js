import './App.css';
import RegisterForm from './RegisterForm'
import React from "react";
import store from "./sagas/store";
import {Provider} from "react-redux";




function App() {
  return (

    <div className="App">
      <Provider store={store}>
        <RegisterForm />
      </Provider>
      </div>

  );
}

export default App;
